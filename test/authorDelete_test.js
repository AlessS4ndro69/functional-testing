 "use strict";

const assert = require("assert");
const { allowedNodeEnvironmentFlags } = require("process");

let webdriver = require("selenium-webdriver");

let By = webdriver.By;
let until = webdriver.until;

describe("Eliminar Autores", () => {
  let driver = new webdriver.Builder()
    .withCapabilities(webdriver.Capabilities.chrome())
    .build();

  it("Eliminar autor sin libros", (done) => {
    driver
      .get("http://localhost:3000/catalog")
      .then(() =>
        driver.findElement(By.xpath("/html/body/div/div/div[1]/ul/li[3]/a"))
      )
      .then((allAuthors) => allAuthors.click())
      .then(() =>
        driver.findElement(By.xpath("/html/body/div/div/div[2]/li[27]/a"))
      )
      .then((autor) => autor.click())
      .then(() =>
        driver.findElement(By.xpath("/html/body/div/div/div[2]/p[2]/a"))
      )
      .then((button) => button.click())
      .then(() =>
        driver.findElement(By.xpath("/html/body/div/div/div[2]/p[2]"))
      )
      .then((message) => message.getText())
      .then((actual) => {
        const expected = "Do you really want to delete this Author?";
        assert.equal(actual, expected);
      })
      .then(() => driver.quit())
      .then(done)
      .catch((err) => done(err));
  });

 /*
  it("Eliminar libro con copias existentes", (done) => {
    driver
      .get("http://localhost:3000/catalog")
      .then(() =>
        driver.findElement(By.xpath("/html/body/div/div/div[1]/ul/li[2]/a"))
      )
      .then((allBooks) => allBooks.click())
      .then(() =>
        driver.findElement(By.xpath("/html/body/div/div/div[2]/ul/li[9]/a"))
      )
      .then((book) => book.click())
      .then(() =>
        driver.findElement(By.xpath("/html/body/div/div/div[2]/p[5]/a"))
      )
      .then((button) => button.click())
      .then(() =>
        driver.findElement(By.xpath("/html/body/div/div/div[2]/p[5]"))
      )
      .then((message) => message.getText())
      .then((text) =>
        assert.equal(
          text,
          "Delete the following copies before attempting to delete this Book."
        )
      )
      .then(() => driver.quit())
      .then(done)
      .catch((err) => done(err));
  });*/
}); 
